From ubuntu:16.04


RUN apt-get update && apt-get install -y wget git cmake g++-4.8 gcc-4.8 libglapi-mesa && apt-get install -y libosmesa6 freeglut3-dev libxft-dev libxmu-dev libxmu-headers xvfb
RUN mkdir /opt/scirun && cd /opt/scirun
WORKDIR /opt/scirun
RUN wget http://www.sci.utah.edu/releases/scirun_v4.7/SCIRun_4.7_20160120_linux.tgz
RUN  tar -xf SCIRun_4.7_20160120_linux.tgz
RUN ./build.sh --cmake-args "-DCMAKE_CXX_COMPILER=/usr/bin/g++-4.8" 
ENV PATH=$PATH:/opt/scirun/bin/
ENV DISPLAY=:100
RUN echo "Xvfb :100 -screen 0 1024x768x16 -ac &" >>~/.bashrc
ADD ./.scirunrc /root/
#RUN apt-get install -y vnc4server
